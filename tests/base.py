import unittest

from selenium import webdriver

import settings
from pages.home.actions import HomePageActions
from pages.login.actions import LoginPageActions
from pages.my_courses.actions import MyCoursePageAction
from pages.course_details.actions import CourseDetailsActions
from pages.complete_purchase.actions import CompletePurchaseActions

class FunctionalTest(unittest.TestCase):
    driver = None
    login_required = False
    _multiprocess_can_split_ = True

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.driver.maximize_window()

        cls.home_page = HomePageActions(cls.driver)
        cls.login_page = LoginPageActions(cls.driver)
        cls.my_courses_page = MyCoursePageAction(cls.driver)
        cls.course_details_page = CourseDetailsActions(cls.driver)
        cls.purchase_course_page = CompletePurchaseActions(cls.driver)

        cls.driver.get(settings.BASE_URL)

        # if cls.home_page.is_friday_pupup_appeared():
        #     cls.home_page.dismiss_friday_sale_popup()

        if cls.home_page.is_get_a_free_java_course_popup_appeared():
            cls.home_page.dismiss_get_a_free_java_course()

        cls.login_page.login_with_email_password()

        cls.my_courses_page.visit_my_courses_page()

        cls.course_details_page.enroll_in_course()

        cls.purchase_course_page.complete_purchase_course()

        cls.purchase_course_page.generate_fake_credit_card_no()

        #
        # if cls.my_courses_page.visit_my_courses_page():
        #     cls.my_courses_page.select_course()

        # if cls.my_courses_page.visit_my_courses_page():
        #     cls.my_courses_page.is_my_courses_page_appeared()

        # if cls.my_courses_page.is_my_courses_top_menu_appeared():
        #     print('login success')
        #
        # if cls.my_courses_page.visit_my_courses_page():
        #     #import pdb; pdb.set_trace()
        #     cls.my_courses_page.is_my_courses_page_appeared()



        # if cls.login_required:
        #     cls.login_page.visit_login_page()


    def setUp(self):
        pass

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(cls):
        print('Test ends')
        # cls.driver.quit()