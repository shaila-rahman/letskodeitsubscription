"""
// *[ @class ='text-center']
h1.text-center
"""
from selenium.webdriver.common.by import By


class LoginPageLocators(object):
    # Top right menu bar
    TOP_MENU_LOGIN_BUTTON = (By.ID, ('DrpDwnMn05label'))

    #Login page
    LOGIN_PAGE_TEXT_CENTER =(By.CLASS_NAME, ('h1.text-center'))
    EMAIL_ADDRESS = (By.ID, 'user_email')
    PASSWORD = (By.ID, 'user_password')
    LOG_IN_BUTTON = (By.NAME, 'commit')


