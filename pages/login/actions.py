from pages.login.locators import LoginPageLocators
from common.webdriver_waits import DriverWaits
from pages.my_courses.locators import MyCoursePageLocators
from settings import LOGIN_CREDENTIAL


class LoginPageActions(object):

    def __init__(self, driver):
        self.driver = driver
        self.waits = DriverWaits(self.driver, timeout=10)

    def login_with_email_password(self):
    #def visit_login_page(self):
        top_menu_login_button = self.driver.find_element(*LoginPageLocators.TOP_MENU_LOGIN_BUTTON)
        top_menu_login_button.click()
        print('Login page appeared')
        # self.waits.wait_till_element_visible(LoginPageLocators.LOGIN_PAGE_TEXT_CENTER)

    # def is_login_page_appeared(self):
    #     try:
    #         self.waits.wait_till_element_visible(LoginPageLocators.LOGIN_PAGE_TEXT_CENTER)
    #     except:
    #         return False
    #     else:
    #         return True

    #def login_with_email_password(self, query_string1, query_string2):
    #def login_with_email_password(self):

        email_address = self.driver.find_element(*LoginPageLocators.EMAIL_ADDRESS)
        email_address.send_keys(LOGIN_CREDENTIAL['email'])

        password = self.driver.find_element(*LoginPageLocators.PASSWORD)
        password.send_keys(LOGIN_CREDENTIAL['password'])

        log_in_button = self.driver.find_element(*LoginPageLocators.LOG_IN_BUTTON)
        log_in_button.click()

    #def is_my_courses_top_menu_appeared(self):
        try:
            self.waits.wait_till_element_is_visible(MyCoursePageLocators.TOP_MENU_LINK_MY_COURSES)
        except:
            return False
        else:
            return True

