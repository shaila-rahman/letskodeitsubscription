# CSS: div#comp-joictytcimg
#xpath: //div[@id='comp-joictytcimg']

from selenium.webdriver.common.by import By


class HomePageLocators(object):
    FRIDAY_SALE_POPUP = (By.ID, 'comp-joictytcimg')
    FRIDAY_SALE_POPUP_CLOSE_BUTTON = (By.ID, 'comp-joictyuz')
    GET_A_FREE_JAVA_COURSE_CLOSE_BUTTON = (By.XPATH, "//*[@viewBox='25.974998474121094 25.975000381469727 148.05001831054688 148.0500030517578']")
    GET_A_FREE_JAVA_COURSE_POPUP = (By.XPATH, "//*[@id='comp-jgmoxwr2inlineContent'][@class='style-jrl6b22yinlineContent']")

