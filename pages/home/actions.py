from pages.home.locators import HomePageLocators
from common.webdriver_waits import DriverWaits

class HomePageActions(object):

    def __init__(self, driver):
        self.driver = driver
        self.waits = DriverWaits(self.driver, timeout=10)

    # def is_friday_pupup_appeared(self):
    #     try:
    #         self.waits.wait_till_element_visible(HomePageLocators.FRIDAY_SALE_POPUP)
    #     except:
    #         return False
    #     else:
    #         return True
    #
    # def dismiss_friday_sale_popup(self):
    #     self.driver.find_element(*HomePageLocators.FRIDAY_SALE_POPUP_CLOSE_BUTTON).click()
    #     self.waits.wait_till_element_is_invisible(HomePageLocators.FRIDAY_SALE_POPUP)

    def is_get_a_free_java_course_popup_appeared(self):
        try:
            self.waits.wait_till_element_visible(HomePageLocators.GET_A_FREE_JAVA_COURSE_POPUP)
        except:
            return False
        else:
            return True

    def dismiss_get_a_free_java_course(self):
        get_a_free_java_course_close_button= self.driver.find_element(*HomePageLocators.GET_A_FREE_JAVA_COURSE_CLOSE_BUTTON)
        self.waits.wait_till_element_is_clickable(HomePageLocators.GET_A_FREE_JAVA_COURSE_CLOSE_BUTTON)
        get_a_free_java_course_close_button.click()
        self.waits.wait_till_element_is_invisible(HomePageLocators.GET_A_FREE_JAVA_COURSE_POPUP)