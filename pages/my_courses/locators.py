from selenium.webdriver.common.by import By


class MyCoursePageLocators (object):

    TOP_MENU_LINK_MY_COURSES = (By.LINK_TEXT, 'My Courses')
    MY_COURSES_PAGE_URL = 'https://learn.letskodeit.com/courses'
    SELECTED_COURSE = (By.XPATH, "//div[@data-course-id='56739']")

    #SEARCH_BUTTON = (By.XPATH, "//*[@id='navbar-submit-button']")
    #// div[ @ data - course - id = '56739']