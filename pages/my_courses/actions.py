from pages.my_courses.locators import MyCoursePageLocators
from common.webdriver_waits import DriverWaits
from pages.course_details.locators import CourseDetailsPageLocator


class MyCoursePageAction(object):

    def __init__(self, driver):
        self.driver = driver
        self.waits = DriverWaits(self.driver, timeout=10)

    def visit_my_courses_page(self):
        top_menu_link_my_courses = self.driver.find_element(*MyCoursePageLocators.TOP_MENU_LINK_MY_COURSES)
        top_menu_link_my_courses.click()

        self.waits.wait_till_url_changes(MyCoursePageLocators.MY_COURSES_PAGE_URL)

        selected_course = self.driver.find_element(*MyCoursePageLocators.SELECTED_COURSE)
        print('My Courses page appeared')

        selected_course.click()
        self.waits.wait_till_url_changes(CourseDetailsPageLocator.COURSE_DETAILS_URL)
        print('Courses selection is successful')

        # try:
        #     self.waits.wait_till_url_cnanges(MyCoursePageLocators.MY_COURSES_PAGE_URL)
        # except:
        #     return False
        # else:
        #     print('My Courses page appeared')
        #     return True

    # def is_my_courses_page_appeared(self):
    #     try:
    #         self.waits.wait_till_url_cnanges(MyCoursePageLocators.MY_COURSES_PAGE_URL)
    #     except:
    #         return False
    #     else:
    #         print('url changes successful')
    #         return True

    # def select_course(self):
    #     selected_course = self.driver.find_element(MyCoursePageLocators.SELECTED_COURSE)
    #     selected_course.click()
    #     self.waits.wait_till_url_changes(CourseDetailsPageLocator.COURSE_DETAILS_URL)