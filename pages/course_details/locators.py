from selenium.webdriver.common.by import By


class CourseDetailsPageLocator(object):
    COURSE_DETAILS_URL = 'https://learn.letskodeit.com/p/python-3-from-scratch'
    COURSE_TITLE = (By.CLASS_NAME, 'course-title')
    COURSE_SUBTITLE = (By.CLASS_NAME, 'course-subtitle')
    COURSE_PRICE = (By.XPATH, "// *[@class ='default-product-price product_342637']")
    COURSE_ENROLL_BUTTON = (By.ID, 'enroll-button-top')