from pages.course_details.locators import CourseDetailsPageLocator
from common.webdriver_waits import DriverWaits
from pages.complete_purchase.locators import CompletePurchaseLocators

class CourseDetailsActions(object):

    def __init__(self, driver):
        self.driver = driver
        self.waits = DriverWaits(self.driver, timeout=10)

    def enroll_in_course(self):
        course_title = self.driver.find_element(*CourseDetailsPageLocator.COURSE_TITLE).text
        print('course_title_test=', course_title)

        course_subtitle = self.driver.find_element(*CourseDetailsPageLocator.COURSE_SUBTITLE).text
        print('course_subtitle=', course_subtitle)

        course_price = self.driver.find_element(*CourseDetailsPageLocator.COURSE_PRICE).text
        print('course_price=', course_price)

        course_enroll_button = self.driver.find_element(*CourseDetailsPageLocator.COURSE_ENROLL_BUTTON)

        course_enroll_button.click()
        self.waits.wait_till_url_changes(CompletePurchaseLocators.COMPLETE_PURCHASE_PAGE_URL)

        return (course_title, course_subtitle, course_price)

