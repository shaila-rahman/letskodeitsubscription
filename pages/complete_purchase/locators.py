from selenium.webdriver.common.by import By


class CompletePurchaseLocators(object):
    COMPLETE_PURCHASE_PAGE_URL = 'https://sso.teachable.com/secure/42299/checkout/342637/python-3-from-scratch'
    ORDER_SUMMARY_TEXT = (By.XPATH, "//*[@class='spc__summary-item spc--text-light']")
    ORDER_SUMMARY_PRICE = (By.XPATH, "//*[@class='spc__summary-item text-right mono']")
    LOGGED_IN_AS = (By.CSS_SELECTOR, 'strong > span')
    CREDIT_CARD_RADIO_BUTTON = (By.ID, 'payment_method_credit_card')
    #CREDIT_CARD_NUMBER_TEXT_BOX = (By.XPATH, "//*[@class='InputElement is-empty'][@name='cardnumber']")
    CREDIT_CARD_iFRAME = (By.XPATH, "//iframe[@name='__privateStripeFrame4']")
    EXPIRATION_DATE_iFRAME = (By.XPATH, "//iframe[@name='__privateStripeFrame5']")
    CVC_CODE_iFRAME = (By.XPATH, "//iframe[@name='__privateStripeFrame6']")
    POSTAL_CODE_iFRAME = (By.XPATH, "//iframe[@name='__privateStripeFrame7']")


    #CREDIT_CARD_NUMBER_TEXT_BOX = (By.XPATH, "//input[@autocomplete='cc-number'][@class='InputElement is-empty']")
    #CREDIT_CARD_NUMBER_TEXT_BOX = (By.XPATH, "//*[@id='root']/form/span[2]/input[1]")
    #CREDIT_CARD_NUMBER_TEXT_BOX = (By.CSS_SELECTOR, '#root > form > span:nth-child(4) > input.InputElement.is-empty')

    CREDIT_CARD_TAB = (By.ID, 'credit-radio')
    PAYPAL_TAB = (By.ID, 'paypal-radio')

    CREDIT_CARD_NUMBER_TEXT_BOX = (By.XPATH, '//*[@name="cardnumber"][@class="InputElement is-empty"]')
    EXPIRATION_DATE_TEXT_BOX = (By.XPATH, "//*[@name='exp-date']")
    CVC_CODE_TEXT_BOX = (By.NAME, 'cvc')
    BILLING_COUNTRY = (By.ID, 'country_code_credit_card-cc')
    POSTAL_CODE = (By.XPATH, "//*[@name='postal'][@class='InputElement is-empty']")
    AGREED_TO_TERMS_CHECKBOX = (By.ID, "agreed_to_terms_checkbox")
    CONFIRM_PURCHASE_BUTTON = (By.ID, 'confirm-purchase')


