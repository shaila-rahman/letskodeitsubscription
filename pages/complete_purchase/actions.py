from faker import Faker
from pages.complete_purchase.locators import CompletePurchaseLocators
import settings
from common.webdriver_waits import DriverWaits


class CompletePurchaseActions(object):

    def __init__(self, driver):
        self.driver = driver
        self.waits = DriverWaits(self.driver, timeout=10)

    def complete_purchase_course(self,):
        order_summary_text = self.driver.find_element(*CompletePurchaseLocators.ORDER_SUMMARY_TEXT).text
        print('order_summary_text=', order_summary_text)

        order_summary_price = self.driver.find_element(*CompletePurchaseLocators.ORDER_SUMMARY_PRICE).text
        print('order_summary_price=', order_summary_price)

        logged_in_as = self.driver.find_element(*CompletePurchaseLocators.LOGGED_IN_AS).text
        print('logged_in_as=', logged_in_as)

        credit_card_radio_button = self.driver.find_element(*CompletePurchaseLocators.CREDIT_CARD_RADIO_BUTTON)

        if credit_card_radio_button.is_selected():
            print('Credit card radion button is selected')
        else:
            print('Credit card radio button is not selected')
            credit_card_tab = self.driver.find_element(*CompletePurchaseLocators.CREDIT_CARD_TAB)
            credit_card_tab.click()
            #credit_card_radio_button.click()

        credit_card_no, credit_card_expiration_date, cvc_code = self.generate_fake_credit_card_no()

        # paypal_tab = self.driver.find_element(*CompletePurchaseLocators.PAYPAL_TAB)
        # paypal_tab.click()

        ############# CREDIT CARD TEXT BOX ############
        self.waits.wait_till_presence_of_element_located(CompletePurchaseLocators.CREDIT_CARD_iFRAME)
        credit_card_iframe = self.driver.find_element(*CompletePurchaseLocators.CREDIT_CARD_iFRAME)
        self.driver.switch_to.frame(credit_card_iframe)
        print('Switched to credit card iframe')

        credit_card_number_text_box = self.driver.find_element(*CompletePurchaseLocators.CREDIT_CARD_NUMBER_TEXT_BOX)
        self.waits.wait_till_element_visible(CompletePurchaseLocators.CREDIT_CARD_NUMBER_TEXT_BOX)
        credit_card_number_text_box.send_keys(credit_card_no)
        self.driver.switch_to.default_content()

        ############# CREDIT CARD EXPIRATION TEXT BOX ############
        self.waits.wait_till_presence_of_element_located(CompletePurchaseLocators.EXPIRATION_DATE_iFRAME)
        expiration_date_iframe = self.driver.find_element(*CompletePurchaseLocators.EXPIRATION_DATE_iFRAME)
        self.driver.switch_to.frame(expiration_date_iframe)
        print('Switched to expiration date iframe')

        expiration_date_text_box = self.driver.find_element(*CompletePurchaseLocators.EXPIRATION_DATE_TEXT_BOX)
        expiration_date_text_box.send_keys(credit_card_expiration_date)
        self.driver.switch_to.default_content()

        ############# CVC CODE TEXT BOX ############
        self.waits.wait_till_presence_of_element_located(CompletePurchaseLocators.CVC_CODE_iFRAME)
        cvc_code_iframe = self.driver.find_element(*CompletePurchaseLocators.CVC_CODE_iFRAME)
        self.driver.switch_to.frame(cvc_code_iframe)
        print('Switched to cvc code iframe')

        cvc_code_text_box = self.driver.find_element(*CompletePurchaseLocators.CVC_CODE_TEXT_BOX)
        cvc_code_text_box.send_keys(cvc_code)
        self.driver.switch_to.default_content()

        ############# POSTSL CODE TEXT BOX ############
        self.waits.wait_till_presence_of_element_located(CompletePurchaseLocators.POSTAL_CODE_iFRAME)
        postal_code_iframe = self.driver.find_element(*CompletePurchaseLocators.POSTAL_CODE_iFRAME)
        self.driver.switch_to.frame(postal_code_iframe)
        print('Switched to postal code iframe')

        postal_code_text_box = self.driver.find_element(*CompletePurchaseLocators.POSTAL_CODE)
        postal_code_text_box.send_keys(settings.POSTAL_CODE)
        self.driver.switch_to.default_content()

    #############  BILLING COUNTRY DROP DOWN ############
        billing_country = self.driver.find_element(*CompletePurchaseLocators.BILLING_COUNTRY)
        billing_country.send_keys(settings.BILLING_COUNTRY)

        agreed_to_terms_checkbox = self.driver.find_element(*CompletePurchaseLocators.AGREED_TO_TERMS_CHECKBOX).click()
        #confirm_purchase_button = self.driver.find_element(*CompletePurchaseLocators.CONFIRM_PURCHASE_BUTTON).click()

        return (order_summary_text, order_summary_price, logged_in_as)

    def generate_fake_credit_card_no(self):
        fake_data = Faker()
        credit_card_no = fake_data.credit_card_number(card_type=None)
        credit_card_expiration_date = fake_data.credit_card_expire(start="now", end="+10y", date_format="%m/%y")
        cvc_code = fake_data.credit_card_security_code(card_type=None)
        return (credit_card_no, credit_card_expiration_date, cvc_code)

        # credit_card_info = fake_data.credit_card_full(card_type=None)
        # print(credit_card_info)

